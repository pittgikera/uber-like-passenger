package com.bishop.uberlike_passenger.ui.welcome

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.bishop.uberlike_passenger.R
import com.bishop.uberlike_passenger.common.*
import com.bishop.uberlike_passenger.ui.main.MainActivity
import com.firebase.ui.auth.AuthUI
import com.firebase.ui.auth.IdpResponse
import kotlinx.android.synthetic.main.activity_welcome.*
import timber.log.Timber

class WelcomeActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_welcome)

        initUi()
    }

    private fun initUi() {
        btn_welcome_start.onClick { initiateLoginFlow() }
    }

    private fun initiateLoginFlow(){
        // Choose authentication providers
        //Passengers to log in with phone
        val providers = arrayListOf(AuthUI.IdpConfig.PhoneBuilder().build())

        // Create and launch sign-in intent
        startActivityForResult(
            AuthUI.getInstance()
                .createSignInIntentBuilder()
                .setAvailableProviders(providers)
                .build(),
            RC_SIGN_IN
        )
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == RC_SIGN_IN) {
            val response = IdpResponse.fromResultIntent(data)

            if (resultCode == Activity.RESULT_OK) {
                // Successfully signed in
                toastySuccess("Welcome to Uber Like")

                MainActivity::class.start(this, true)
                // ...
            } else {
                // Sign in failed. If response is null the user canceled the
                // sign-in flow using the back button. Otherwise check
                // response.getError().getErrorCode() and handle the error.
                // ...
                response?.let {
                    Timber.e("Sign in error ${it.error?.errorCode}")

                    toastyError("Could not sign in at this time. ${it.error?.localizedMessage}")
                }
            }
        }
    }
}

package com.bishop.uberlike_passenger.ui.splash

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.bishop.uberlike_passenger.common.start
import com.bishop.uberlike_passenger.ui.main.MainActivity
import com.bishop.uberlike_passenger.ui.welcome.WelcomeActivity
import com.google.firebase.auth.FirebaseAuth

class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        checkIfAuthenticated()
    }

    private fun checkIfAuthenticated() {
        val user = FirebaseAuth.getInstance().currentUser

        if (user != null) {
            MainActivity::class.start(this, true)
        } else {
            WelcomeActivity::class.start(this, true)
        }
    }
}

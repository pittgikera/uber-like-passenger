package com.bishop.uberlike_passenger.ui.main

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.bishop.uberlike_passenger.R
import com.bishop.uberlike_passenger.common.DRIVERS_COLLECTION
import com.bishop.uberlike_passenger.model.Driver
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.ListenerRegistration
import kotlinx.android.synthetic.main.activity_maps.*
import timber.log.Timber

class MapsActivity : AppCompatActivity(), OnMapReadyCallback {

    private lateinit var mMap: GoogleMap
    private val db = FirebaseFirestore.getInstance()
    private lateinit var mRegister: ListenerRegistration
    private val mOnlineDrivers = ArrayList<Driver>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)
        setSupportActionBar(toolbar_map)
        supportActionBar?.setDefaultDisplayHomeAsUpEnabled(true)

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        // Add a marker in Sydney and move the camera
        val sydney = LatLng(-34.0, 151.0)
        mMap.addMarker(MarkerOptions().position(sydney).title("Marker in Sydney"))
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney))

        trackOnlineDrivers()
    }

    private fun trackOnlineDrivers() {
        val query = db.collection(DRIVERS_COLLECTION)
        mRegister = query.addSnapshotListener { value, e ->
            if (e != null) {
                Timber.w("Listen failed. $e")
                return@addSnapshotListener
            }

            mOnlineDrivers.clear()
            for (doc in value!!) {
                mOnlineDrivers.add(doc.toObject(Driver::class.java))
            }
            updateMarkers()
            Timber.d("Current cites in CA: $mOnlineDrivers")
        }
    }

    private fun updateMarkers() {
        mMap.apply {
            clear()
            mOnlineDrivers.forEach {
                val cord = it.location.coordinates

                addMarker(MarkerOptions()
                    .position(LatLng(cord[0], cord[1]))
                    .title(it.name)
                )
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        mRegister.remove()
    }
}

package com.bishop.uberlike_passenger.ui.main

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.lifecycle.Observer
import com.bishop.uberlike_passenger.App
import com.bishop.uberlike_passenger.R
import com.bishop.uberlike_passenger.common.onClick
import com.bishop.uberlike_passenger.common.start
import com.bishop.uberlike_passenger.data.PreferenceRepository
import com.bishop.uberlike_passenger.ui.splash.SplashActivity
import com.firebase.ui.auth.AuthUI
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*

class MainActivity : AppCompatActivity() {
    private lateinit var preferenceRepository: PreferenceRepository
    private lateinit var firebaseUser: FirebaseUser

    private lateinit var rootLayout: CoordinatorLayout
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        preferenceRepository = (application as App).preferenceRepository
        rootLayout = findViewById(R.id.cl_main_root)

        FirebaseAuth.getInstance().currentUser?.let {
            firebaseUser = it
        }

        initUi()
    }

    private fun initUi() {
        preferenceRepository.nightModeLive.observe(this, Observer { nightMode ->
            nightMode?.let { delegate.localNightMode = it }
        }
        )

        if (::firebaseUser.isInitialized) {
            tv_main_salutations?.text =
                getString(R.string.home_salutations, firebaseUser.displayName ?:"")
        }

        btn_main_trackDriver?.onClick {
            MapsActivity::class.start(this)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            R.id.action_sign_out -> {
                AuthUI.getInstance()
                    .signOut(this)
                    .addOnCompleteListener {
                        // ...
                        SplashActivity::class.start(this, true)
                    }
                true
            }
            R.id.action_switch_theme -> {
                preferenceRepository.isDarkTheme = !preferenceRepository.isDarkTheme
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}

package com.bishop.uberlike_passenger.model

class Driver {
    var name: String? = null
    var location = GeoLocation()
}

data class GeoLocation(
    val type:String = "Point",
    val coordinates:ArrayList<Double> = arrayListOf()
)